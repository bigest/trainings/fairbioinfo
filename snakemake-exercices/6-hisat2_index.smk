fastaFile = "Data/O.tauri_genome.fna"
genome = "Otauri"

rule all:
    input:
        expand("index_{genome}", genome=genome)

rule hisat2_index:
    input:
        fasta = fastaFile
    output:
        directory("index_{genome}")
    params:
        prefix = "index_{genome}/"
    log:
        "logs/hisat2_index_{genome}.log"
    threads: 2
    wrapper:
        "v3.7.0/bio/hisat2/index"
