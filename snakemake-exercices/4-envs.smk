SAMPLES, = glob_wildcards("Data/{sample}.fastq.gz")
HISAT2idx = "hisat2_indexes/Otauri"

rule all:
    input:
        expand("results/mapping/{sample}.sam", sample=SAMPLES)

rule hisat2:
    input:
        "Data/{sample}.fastq.gz"
    output: 
        "results/mapping/{sample}.sam" 
    envmodules:
        "hisat2/2.2.1"
    conda:
        "envs/hisat2.yml"
    container:
        "docker://quay.io/biocontainers/hisat2:2.2.1--hdbdd923_6" 
    log:
        "results/mapping/{sample}.log"
    message: """--- Mapping with hisat2 {input}."""
    shell:
        "hisat2 -x {HISAT2idx} -q -U {input} -S {output} 2> {log}"
