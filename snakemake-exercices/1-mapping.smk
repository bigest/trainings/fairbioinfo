rule hisat2:
    input:
        "Data/SRR3099585_chr18.fastq.gz"
    output: 
        "hisat2/SRR3099585_chr18.sam"  
    params:
        "hisat2_indexes/Otauri"
    message: """--- Mapping with hisat2 {input}."""
    shell:
        "hisat2 -x {params} -q -U {input} -S {output}"
