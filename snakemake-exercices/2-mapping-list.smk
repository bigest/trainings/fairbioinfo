SAMPLES = ["SRR3099585_chr18","SRR3099586_chr18","SRR3099587_chr18"]
HISAT2idx = "hisat2_indexes/Otauri"

rule all:
  input:
    expand("results/mapping/{sample}.sam", sample=SAMPLES)

rule hisat2:
  input:
    "Data/{sample}.fastq.gz"
  output: 
    "results/mapping/{sample}.sam"
  message: """--- Mapping with hisat2 {input}."""
  shell:
    "hisat2 -x {HISAT2idx} -q -U {input} -S {output}"
