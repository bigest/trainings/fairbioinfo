SAMPLES, = glob_wildcards("Data/{sample}.fastq.gz")

rule all:
    input:
        expand("results/quality/{sample}_fastqc.html", sample=SAMPLES),
        expand("results/quality/{sample}_fastqc.zip", sample=SAMPLES)

rule fastqc:
    input:
        "Data/{sample}.fastq.gz"
    output:
        html="results/quality/{sample}_fastqc.html",
        zip="results/quality/{sample}_fastqc.zip" 
    params:
        extra = "--quiet"
    log:
        "logs/fastqc/{sample}.log"
    threads: 1
    resources:
        mem_mb = 1024
    wrapper:
        "v3.7.0/bio/fastqc"
        