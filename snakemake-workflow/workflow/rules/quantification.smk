rule htseq:
    input:
        expand(config['res'] + "mapping/{sample}.bam", sample = SAMPLES)
    output:
        config['res'] + "quantification/htseq-counts.txt"
    params:
        config['gff']
    log:
        config['log'] + "htseq-counts.log"
    conda:
        "../envs/htseq-0.13.5.yml"
    container:
        "https://depot.galaxyproject.org/singularity/htseq:0.13.5--py39h70b41aa_1" # "docker://quay.io/biocontainers/htseq:0.13.5--py39h70b41aa_1" # https://depot.galaxyproject.org/singularity/htseq%3A2.0.5--py39hd5189a5_0
    envmodules:
        "htseq/0.13.5"
    shell: 
        "htseq-count -f bam -r pos -s no -t gene -i ID -m intersection-nonempty {input} {params} > {output} 2> {log}" 
