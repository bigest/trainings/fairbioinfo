rule fastqc:
    input:
        config['libraries'] + "{sample}.fastq.gz" 
    output:
        html= config['res'] + "quality/{sample}_fastqc.html",
        zip= config['res'] + "quality/{sample}_fastqc.zip"
    envmodules:
        "fastqc/0.12.1"
    conda:
        "../envs/fastqc-0.12.1.yml"
    container:
        "docker://quay.io/biocontainers/fastqc:0.12.1--hdfd78af_0" 
    params:
        config['res'] + "quality"
    log:
        config['log'] + "fastqc/{sample}.log"
    threads: 1
    resources:
        mem_mb = 1024
    message: """--- Quality with FastQC {input}."""
    shell:
        "fastqc --outdir {params} {input} > {log}"

rule multiqc:
    input:
        expand(config['res'] + "quality/{sample}_fastqc.zip", sample = SAMPLES),
        rules.htseq.output,
        expand(config['res'] + "mapping/{sample}.bam", sample = SAMPLES) # not mandatory, input de htseq
    output:
        config['res'] + "multiqc_report.html",
        directory(config['res'] + "multiqc_data")
    params:
        config['res']
    log:
        config['log'] + "multiqc.log"
    conda:
        "../envs/multiqc-1.13.yml"
    container:
        "https://depot.galaxyproject.org/singularity/multiqc:1.13--pyhdfd78af_0"
    envmodules:
        "multiqc/1.13"
    shell: 
        "multiqc {params} -f -o {params} >{log}" 
