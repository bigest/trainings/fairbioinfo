rule deseq:
    input:
        counts = rules.htseq.output
    output:
        csv = config['res'] + "DESeq2/DE_results.csv",
        png = config['res'] + "DESeq2/deseq2_demo.png"
    envmodules:
        "r/4.3.1"
    conda:
        "../envs/deseq2.yml"
    container:
        "docker://quay.io/biocontainers/bioconductor-deseq2:1.42.0--r43hf17093f_0" # "https://depot.galaxyproject.org/singularity/bioconductor-deseq:1.42.0--r43hf17093f_0"
    log:
        config['log'] + "deseq.log"
    message: """--- deseq2 {input}."""
    script:
        "../scripts/DESeq2.R"
