rule hisat2_index:
    input: 
        config['fasta']
    output:
        expand(f"{{idx_path}}.{{ix}}.ht2", idx_path = config['hisat2_idx'], ix=range(1, 9))
    params:
        idx = config['hisat2_idx']
    envmodules:
        "hisat2/2.2.1"
    conda:
        "../envs/hisat2-2.2.1.yml"
    container:
        "docker://quay.io/biocontainers/hisat2:2.2.1--hdbdd923_6" 
    threads: 2
    log:
        "results/logs/hisat2-build.log"
    shell: 
        '''
        hisat2-build -f -p {threads} {input} {params.idx} > {log}
        '''

rule hisat2:
    input:
        fq = config['libraries'] + "{sample}.fastq.gz",
        idx = expand(f"{{idx_path}}.{{ix}}.ht2", idx_path = config['hisat2_idx'], ix=range(1, 9))
    output: 
        config['res'] + "mapping/{sample}.sam" 
    params:
        idx = config['hisat2_idx'],
        sum = config['res'] + "mapping/{sample}.summary.txt"
    envmodules:
        "hisat2/2.2.1"
    conda:
        "../envs/hisat2-2.2.1.yml"
    container:
        "https://depot.galaxyproject.org/singularity/hisat2%3A2.2.1--hdbdd923_6" # "docker://quay.io/biocontainers/hisat2:2.2.1--hdbdd923_6" 
    threads: 2
    resources:
        mem_mb = 2048
    log:
        config['log'] + "hisat2/{sample}.log"
    message: """--- Mapping with hisat2 {input}."""
    shell:
        "hisat2 -p {threads} -x {params.idx} -q -U {input.fq} -S {output} --summary-file {params.sum} --new-summary 2>{log}"

rule samtools:
    input:
        rules.hisat2.output
    output: 
        bam = config['res'] + "mapping/{sample}.bam",
        bai = config['res'] + "mapping/{sample}.bam.bai"
    envmodules:
        "samtools/1.18"
    conda:
        "../envs/samtools-1.18.yml"
    container:
        "https://depot.galaxyproject.org/singularity/samtools:1.18--h50ea8bc_1" # "docker://quay.io/biocontainers/samtools:1.18--h50ea8bc_1" 
    threads: 1
    resources:
        mem_mb = 2048
    message: """--- Samtools sort and index {input}"""
    shell:
        """
        samtools view -b {input} | samtools sort -o {output.bam} - 
        samtools index {output.bam}
        """
