This workflow performs an analysis on single--end RNA-seq data.
After checking quality with `FastQC <https://www.bioinformatics.babraham.ac.uk/projects/fastqc/>`_, reads were mapped with `Hisat2 <https://daehwankimlab.github.io/hisat2/>`_ and gene counts were generated with `HTseq-count <https://htseq.readthedocs.io/>`_.
Integrated normalization and differential expression analysis was conducted with `DESeq2 <https://bioconductor.org/packages/release/bioc/html/DESeq2.html>`_.
