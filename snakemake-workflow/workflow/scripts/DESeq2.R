log <- file(snakemake@log[[1]], open = "wt")
sink(log)
sink(log, type="message")

library("DESeq2")

cts <- read.table(snakemake@input[["counts"]], sep="\t",row.names=1)
colnames(cts) = c(paste0("condition1_",1:3),paste0("condition2_",1:3))

coldata = cbind.data.frame(condition= c(rep("condition1", 3),rep("condition2", 3)))
rownames(coldata) = c(paste0("condition1_",1:3),paste0("condition2_",1:3))

dds <- DESeqDataSetFromMatrix(countData = cts,
                              colData = coldata ,
                              design= ~ condition)

# remove uninformative columns
dds <- dds[rowSums(counts(dds)) > 1, ]

dds <- DESeq(dds)
#resultsNames(dds) # lists the coefficients


res <- results(dds, name="condition_condition2_vs_condition1")
write.csv(res, file = snakemake@output[["csv"]])

png(file = snakemake@output[["png"]], width = 800, height = 700)
plotMA(res, ylim=c(-2,2))
dev.off()


