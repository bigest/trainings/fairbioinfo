# Snakemake workflow: `<name>`

[![Snakemake](https://img.shields.io/badge/snakemake-≥6.3.0-brightgreen.svg)](https://snakemake.github.io)
[![GitHub actions status](https://github.com/<owner>/<repo>/workflows/Tests/badge.svg?branch=main)](https://github.com/<owner>/<repo>/actions?query=branch%3Amain+workflow%3ATests)


A Snakemake workflow for `FAIR-bioinfo2024 training`


## Usage

Download and decompress the data from https://zenodo.org/records/3997237 in resources directory.  

Change the paths in config/config.yml before running the workflow.

`snakemake -j 6 --sdm <conda, apptainer or env-modules>`  

## Output

The workflow will :
- check the quality of the raw data with FastQC
- create hisat2 indexes for the reference genome
- map the reads on the genome with hisat2
- convert, sort and index the mapping files
- count the number of reads per gene with htseq-count
- normalize the data with DESeq2
- run a report with multiQC





