#!/bin/bash

#SBATCH --cpus-per-task=4
#SBATCH --mem=1G
#SBATCH --time=05:00
#SBATCH --output=alignment-%j.out
#SBATCH --error=alignment-%j.err
#SBATCH --mail-type=END,FAIL
#SBATCH --mail-user=mon@email.fr

module load hisat2/2.2.1 samtools/1.18

reference_index="hisat2_indexes/Otauri"
file_id="SRR3099585_chr18"

mkdir -p hisat2

srun hisat2 --threads="${SLURM_CPUS_PER_TASK}" -x "${reference_index}" -q -U "Data/${file_id}.fastq.gz" -S hisat2/${file_id}.sam
srun -J "filter" samtools view -b -o "hisat2/${file_id}.bam" "hisat2/${file_id}.sam"
srun -J "sort" samtools sort -o "hisat2/${file_id}-sort.bam" "hisat2/${file_id}.bam"
srun -J "index" samtools index "hisat2/${file_id}-sort.bam"

rm "hisat2/${file_id}.sam" "hisat2/${file_id}.bam"