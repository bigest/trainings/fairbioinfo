#!/bin/bash

module load hisat2 samtools

reference_index="hisat2_indexes/Otauri"
file_id="SRR3099585_chr18"

mkdir -p hisat2

srun hisat2 --threads=4 -x "${reference_index}" -q -U "Data/${file_id}.fastq.gz" -S hisat2/${file_id}.sam
srun samtools view -b -o "hisat2/${file_id}.bam" "hisat2/${file_id}.sam"
srun samtools sort -o "hisat2/${file_id}-sort.bam" "hisat2/${file_id}.bam"
srun samtools index "hisat2/${file_id}-sort.bam"