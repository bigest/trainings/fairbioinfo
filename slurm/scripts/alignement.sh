#!/bin/bash

#SBATCH --array=0-5
#SBATCH --cpus-per-task=4
#SBATCH --mem=1G

module load hisat2/2.2.1 samtools/1.18

reference_index="hisat2_indexes/Otauri"
files=(Data/*fastq.gz)
file_id=$(basename -s .fastq.gz "${files[$SLURM_ARRAY_TASK_ID]}")

mkdir -p hisat2

srun -J "${file_id} hisat2" hisat2 --threads="${SLURM_CPUS_PER_TASK}" -x "${reference_index}" -q -U "Data/${file_id}.fastq.gz" -S hisat2/${file_id}.sam
srun -J "${file_id} filter" samtools view -b -o "hisat2/${file_id}.bam" "hisat2/${file_id}.sam"
srun -J "${file_id} sort" samtools sort -o "hisat2/${file_id}-sort.bam" "hisat2/${file_id}.bam"
srun -J "${file_id} index" samtools index "hisat2/${file_id}-sort.bam"

rm "hisat2/${file_id}.sam" "hisat2/${file_id}.bam"